FROM openjdk:7-jre-alpine


WORKDIR /app

COPY target/myproject-0.0.1-SNAPSHOT.jar

COPY ds/run.sh /app/run.sh

RUN chmod +x /app/run.sh

EXPOSE 8080
ENTRYPOINT ["./run.sh"]
